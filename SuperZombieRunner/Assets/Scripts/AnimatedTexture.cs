﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatedTexture : MonoBehaviour
{
    private const string TextureName = "_MainTex";

    public Vector2 speed = Vector2.zero;
    private Vector2 offset = Vector2.zero;

    private Material material;

    void Start ()
    {
        material = GetComponent<Renderer>().material;

        offset = material.GetTextureOffset(TextureName);
    }

	void Update ()
	{
	    offset += speed * Time.deltaTime;

        material.SetTextureOffset(TextureName, offset);
	}
}
