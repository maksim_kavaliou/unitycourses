﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TiledBackground : MonoBehaviour
{
    public int textureSize = 32;

    public bool scaleHorizontially = true;
    public bool scaleVertically = true;

	void Start ()
	{
	    var newWidth = scaleHorizontially ? Mathf.Ceil(Screen.width / (textureSize * PixilPerfectCamera.scale)) : 1;
        var newHeight = scaleVertically ? Mathf.Ceil(Screen.height / (textureSize * PixilPerfectCamera.scale)) : 1;

        transform.localScale = new Vector3(newWidth * textureSize, newHeight * textureSize, 1);

        GetComponent<Renderer>().material.mainTextureScale = new Vector3(newWidth, newHeight, 1);
    }
}
