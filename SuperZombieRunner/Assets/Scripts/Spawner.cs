﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject[] prefabs;
    public float delay = 2;
    public bool active = true;
    public Vector2 delayRange = new Vector2(1, 3);

	void Start ()
	{
	    ResetDelay();
        StartCoroutine(EnemyGenerator());
	}

    IEnumerator EnemyGenerator()
    {
        yield return new WaitForSeconds(delay);

        if (active)
        {
            var newTransform = transform;

            Instantiate(this.prefabs[Random.Range(0, this.prefabs.Length)], newTransform.position, Quaternion.identity);
            ResetDelay();
        }

        StartCoroutine(EnemyGenerator());
    }

    void ResetDelay()
    {
        this.delay = Random.Range(this.delayRange.x, this.delayRange.y);
    }
}
