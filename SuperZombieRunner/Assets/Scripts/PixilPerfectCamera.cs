﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PixilPerfectCamera : MonoBehaviour
{
    public static float pixelsToUnit = 1;

    public static float scale = 1;

    public Vector2 nativeResolution = new Vector2(240, 160);

    void Awake ()
    {
        var camera = GetComponent<Camera>();

        if (camera.orthographic)
        {
            scale = Screen.height / nativeResolution.y;
            pixelsToUnit *= scale;
            camera.orthographicSize = (Screen.height / 2.0f) / pixelsToUnit;
        }
    }
}
